# Apereo CAS Authentication for Laravel

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NU3XK7VXYTYKY)
[![Latest Stable Version](https://poser.pugx.org/sentrasoft/laravel-cas/v/stable)](https://packagist.org/packages/sentrasoft/laravel-cas)
[![Total Downloads](https://poser.pugx.org/sentrasoft/laravel-cas/downloads)](https://packagist.org/packages/sentrasoft/laravel-cas)
[![Monthly Downloads](https://poser.pugx.org/sentrasoft/laravel-cas/d/monthly)](https://packagist.org/packages/sentrasoft/laravel-cas)
[![Latest Unstable Version](https://poser.pugx.org/sentrasoft/laravel-cas/v/unstable)](https://packagist.org/packages/sentrasoft/laravel-cas)
[![License](https://poser.pugx.org/sentrasoft/laravel-cas/license)](https://packagist.org/packages/sentrasoft/laravel-cas)

Easy Bring to CAS Authentication for Laravel

## Install

#### Via Composer

``` php
composer config repositories.maitrepylos vcs https://gitlab.com/maitrepylos/laravel-cas.git
composer require maitrepylos/laravel-cas
```

#### Via edit `composer.json`

	"require": {
		"maitrepylos/laravel-cas": "dev-master"
	}

Next, update Composer from the Terminal:

``` bash
$ composer update
```

## Configuration 
***(Not necessary in Laravel > 5.5)***

After updating composer, add the ServiceProvider to the providers array in `config/app.php`.

```php
'providers' => array(
    .....
    Maitrepylos\Cas\CasServiceProvider::class,
);
```

Now add the alias in `config/app.php`.

```php
'aliases' => array(
    ......
    'Cas' => Maitrepylos\Cas\Facades\Cas::class,
);
```

Add the middelware to your `Kernel.php` file or leverage your own:
```php
'cas.auth'  => \Maitrepylos\Cas\Middleware\Authenticate::class,
'cas.guest' => \Maitrepylos\Cas\Middleware\RedirectIfAuthenticated::class,
'chose_login' => \Maitrepylos\Cas\Middleware\ChooseLoginUrl::class,
```

#### Now publish the configuration file:
``` php
// config/cas.php
// Httpp/Controllers/Auth/CasController.php
// database/migrations/add_sheldon_to_user.php

$ php artisan vendor:publish --provider="Maitrepylos\Cas\CasServiceProvider" 
```

Add new environment variables below to your `.env`
```
CAS_ENABLE=true
CAS_PORT = 443
CAS_HOSTNAME='auth.henallux.be'
CAS_URI='/identity/datt-local'
CAS_DEBUG=true
CAS_REDIRECT_PATH=http://datt-local.henallux.be/cas/login
CAS_LOGOUT_URL=https://auth.henallux.be/identity/datt-local/logout
CAS_LOGOUT_REDIRECT=https://henallux.be
```
Add the variables for API
```
API_SHELDON_URL=https://api.henallux.be/v1/
API_SHELDON_PSEUDO=user
API_SHELDON_PASSWORD=password
```

> To see further configuration, please see and read the description for each configuration item [config/cas.php](src/Config/cas.php)

## Route

#### Authentication
Redirect the user to the authentication page for the provider.
```php
Route::get('/cas/login', function() {
    return cas()->authenticate();
})->name('cas.login');

```

#### Controller and Callback Route
You can create a new controller named `Auth\CasController`.
***(Normally published automatically)***
```php
php artisan make:controller Auth\CasController
```

```php
class CasController extends Controller
{
    /**
     * Obtain the user information from CAS.
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function callback()
    {
        // $username = Cas::getUser();
        // Here you can store the returned information in a local User model on your database (or storage).

        // This is particularly usefull in case of profile construction with roles and other details
        // e.g. Auth::login($local_user);

        return redirect()->route('home');
    }
}
```

When the authentication is performed the callback url is invoked. In that callback you can process the User and create a local entry in the database.
```php
Route::get('/cas/callback', 'Auth\CasController@callback')->name('cas.callback');
```

#### Logout
Logout of the CAS Session and redirect users.

```php
Route::post('/cas/logout', [ 'middleware' => 'cas.auth', function() {
    cas()->logout();

    // You can also add @param string $url in param[0]
    cas()->logout(url('/'));

    // Or add @param string $service in param[1]
    cas()->logout('', url('/'));

}])->name('cas.logout');
```

>The `cas.auth` middleware is optional, but you will need to handle the error when a user tries to logout when they do not have a CAS Session.

> If the `CAS_LOGOUT_REDIRECT` configuration item in `.env` is added, the value is taken from that configuration. Or if nothing is configured, the value is taken based on the value you specified.


If you want to use *SLO (Single Logout)* (if the CAS server supports SLO), Your application must have a valid SSL and the CAS server must be able to send *HTTP POST* `/cas/logout` without having to verify `CsrfToken`. Therefore, you must change the `App\Http\Middleware\VerifyCsrfToken` file and exclude `/cas/logout` route.

```php
/**
 * The URIs that should be excluded from CSRF verification.
 *
 * @var array
 */
protected $except = [
    //

    '/cas/logout',
];
```

You can check that it works by trying to send an *HTTP POST* via [cURL](https://en.wikipedia.org/wiki/CURL).
```
curl -X POST https://yourapp.com/cas/logout
```

## Usage

#### Get User
To retrieve authenticated credentials.

> Not ID (integer), but given on the CAS login (username) form.

```php
$uid = Cas::user()->id;
```

#### Get User Attributes
Get the attributes for for the currently connected user.
```php
foreach (Cas::user()->getAttributes() as $key => $value) {
	...
}
```

#### Get User Attribute
Retrieve a specific attribute by key name. The attribute returned can be either a string or an array based on matches.
```php
$value = Cas::user()->getAttribute('key');
```

## Usage Trait AfterCasAuth

```php
$this->getUsername() // mdpernag
$this->getEmail() // mdpernag@henallux.be
$this->getAttributes() // All atrributes
$this->getAttribute(name) // finds the attribute
$this->{attribute} // finds the attribute
$this->createUser( [$check_if_exist = false]) // Create a user on the Laravel model user, if $check_if_exist = true, check if existing in DB
$this->saveUser($user) // save $user in DB
$this->addNumproecoToUser($user,[ $save_or_not = false]) // add matricule proéco in attribut $user
$this->addPeopleIdToUser($user, [$save_or_not = false])  // add id_people Sheldon in attribut $user
$this->addNamesToUser($user, [$save_or_not = false]) // // add nom (firtsname), prenom(lastname) Sheldon in attribut $user
$this->authInLaravel($user) // Auth Laravel
$this->authInBackpack($user) // Auth BackPack
$this->getSheldonApiDatas($url, [$method = 'GET']) // Returns the result of a REST request to API Sheldon, in the form of an array
$this->addSheldonDatasToUser($user, $url, [$save_or_not = false]) // Adds as attribute in $user, the result of the first array of a REST query to the Sheldon API
```
